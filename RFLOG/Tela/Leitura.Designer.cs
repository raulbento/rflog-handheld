﻿namespace RFLOG.Tela
{
    /// <summary>
    ///     Tela faz as leituras das Tags.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    partial class Leitura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.back = new System.Windows.Forms.Button();
            this.next = new System.Windows.Forms.Button();
            this.quantidade = new System.Windows.Forms.Label();
            this.labelQuantidade = new System.Windows.Forms.Label();
            this.listBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // back
            // 
            this.back.BackColor = System.Drawing.Color.Red;
            this.back.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.back.ForeColor = System.Drawing.SystemColors.Control;
            this.back.Location = new System.Drawing.Point(0, 195);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(118, 36);
            this.back.TabIndex = 1;
            this.back.Text = "<";
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // next
            // 
            this.next.BackColor = System.Drawing.Color.DodgerBlue;
            this.next.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.next.ForeColor = System.Drawing.SystemColors.Control;
            this.next.Location = new System.Drawing.Point(119, 195);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(118, 36);
            this.next.TabIndex = 2;
            this.next.Text = ">";
            this.next.Click += new System.EventHandler(this.next_Click);
            // 
            // quantidade
            // 
            this.quantidade.BackColor = System.Drawing.Color.NavajoWhite;
            this.quantidade.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.quantidade.ForeColor = System.Drawing.Color.DarkOrange;
            this.quantidade.Location = new System.Drawing.Point(119, 170);
            this.quantidade.Name = "quantidade";
            this.quantidade.Size = new System.Drawing.Size(115, 22);
            this.quantidade.Text = "0";
            this.quantidade.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelQuantidade
            // 
            this.labelQuantidade.BackColor = System.Drawing.Color.Transparent;
            this.labelQuantidade.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.labelQuantidade.ForeColor = System.Drawing.Color.OliveDrab;
            this.labelQuantidade.Location = new System.Drawing.Point(4, 170);
            this.labelQuantidade.Name = "labelQuantidade";
            this.labelQuantidade.Size = new System.Drawing.Size(114, 22);
            this.labelQuantidade.Text = "Quantidade";
            this.labelQuantidade.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // listBox
            // 
            this.listBox.BackColor = System.Drawing.Color.White;
            this.listBox.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Italic);
            this.listBox.ForeColor = System.Drawing.Color.Black;
            this.listBox.Location = new System.Drawing.Point(4, 4);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(230, 162);
            this.listBox.TabIndex = 10;
            // 
            // Leitura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(237, 231);
            this.ControlBox = false;
            this.Controls.Add(this.listBox);
            this.Controls.Add(this.labelQuantidade);
            this.Controls.Add(this.quantidade);
            this.Controls.Add(this.next);
            this.Controls.Add(this.back);
            this.MinimizeBox = false;
            this.Name = "Leitura";
            this.Text = "RFLOG - Leitura";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button next;
        private System.Windows.Forms.Label quantidade;
        private System.Windows.Forms.Label labelQuantidade;
        private System.Windows.Forms.ListBox listBox;

    }
}

