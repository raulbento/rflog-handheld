﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Symbol.ResourceCoordination;
using Symbol.RFID3;

namespace RFLOG.Tela
{
    public partial class AlertaLeitura : Form
    {

        int time = 0;

        [DllImport("CoreDll.dll")]
        private static extern void MessageBeep(int code);

        public AlertaLeitura()
        {
            InitializeComponent();
            if (Trigger != null)
            {
                Trigger.Stage2Notify += new Trigger.TriggerEventHandler(Trigger_Stage2Notify);
            }
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
        }

        private Trigger trigger = null;

        private Trigger Trigger
        {
            get
            {
                if (trigger == null)
                {
                    var available = TriggerDevice.AvailableTriggers;

                    foreach (TriggerDevice triggerDevice in available)
                    {
                        if (triggerDevice.ID == TriggerID.TRIGGER3)
                        {
                            trigger = new Trigger(triggerDevice);

                            break;
                        }
                    }
                }
                return trigger;
            }
        }

        private RFIDReader _RFIDReader = null;

        private RFIDReader RFIDReader
        {
            get
            {
                if (_RFIDReader == null || !_RFIDReader.IsConnected)
                {
                    _RFIDReader = new RFIDReader("127.0.0.1", 0, 0);
                    try
                    {
                        _RFIDReader.Connect();
                    }
                    catch
                    {
                        return null;
                    }
                    _RFIDReader.Events.AttachTagDataWithReadEvent = true;
                    _RFIDReader.Events.ReadNotify += RFIDReader_ReadNotify;
                }
                return _RFIDReader;
            }
        }

        private List<string> _TagsFound = null;

        private List<string> TagsFound
        {
            get
            {
                if (_TagsFound == null)
                {
                    _TagsFound = new List<string>();
                }
                return _TagsFound;
            }
            set
            {
                _TagsFound = value;
            }
        }

        private void StartRFIDRead()
        {
            TagsFound = null;
            try
            {
                RFIDReader.Actions.Inventory.Perform();
            }
            catch
            {

            }
        }

        private void StopRFIDRead()
        {
            if (RFIDReader != null)
            {
                try
                {
                    RFIDReader.Actions.Inventory.Stop();
                }
                finally
                {
                    //
                }
            }
        }

        void Trigger_Stage2Notify(object sender, TriggerEventArgs e)
        {
            if (e.NewState == TriggerState.STAGE2)
            {
                StartRFIDRead();
                timer.Enabled = true;
                RFLOG.AlertaLeitura.Show();
            }
            else
            {
                StopRFIDRead();
                foreach (string value in TagsFound)
                {
                    if (!RFLOG.Leitura.ListaTags.Items.Contains(value))
                    {
                        RFLOG.Leitura.ListaTags.Items.Add(value);
                        RFLOG.Leitura.ListaTags.DisplayMember = value;
                    }
                }
                RFLOG.Leitura.Quantidade.Text = RFLOG.Leitura.ListaTags.Items.Count.ToString();
                timer.Enabled = false;
                tempo.Text = (time = 0).ToString();
                RFLOG.Leitura.Show();
            }
        }

        void RFIDReader_ReadNotify(object obj, Events.ReadEventArgs args)
        {
            var id = args.ReadEventData.TagData.TagID as string;
            if (!TagsFound.Contains(id))
            {
                TagsFound.Add(id);
                MessageBeep(-1);
            }
        }

        private RFLOG rflog;

        public RFLOG RFLOG
        {
            get
            {
                if (rflog == null)
                {
                    rflog = new RFLOG();
                }
                return rflog;
            }

            set
            {
                rflog = value;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            tempo.Text = (++time).ToString();
        }

    }
}