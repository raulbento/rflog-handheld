﻿namespace RFLOG.Tela
{
    /// <summary>
    ///     Tela inicial das atividades do sistema.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    partial class Inicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkout = new System.Windows.Forms.Button();
            this.fechar = new System.Windows.Forms.Button();
            this.atualizar = new System.Windows.Forms.Button();
            this.inventario = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // checkout
            // 
            this.checkout.BackColor = System.Drawing.Color.DodgerBlue;
            this.checkout.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.checkout.ForeColor = System.Drawing.Color.White;
            this.checkout.Location = new System.Drawing.Point(0, 116);
            this.checkout.Name = "checkout";
            this.checkout.Size = new System.Drawing.Size(118, 110);
            this.checkout.TabIndex = 6;
            this.checkout.Text = "Sobre";
            // 
            // fechar
            // 
            this.fechar.BackColor = System.Drawing.Color.DarkOrange;
            this.fechar.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.fechar.ForeColor = System.Drawing.Color.White;
            this.fechar.Location = new System.Drawing.Point(119, 116);
            this.fechar.Name = "fechar";
            this.fechar.Size = new System.Drawing.Size(118, 110);
            this.fechar.TabIndex = 5;
            this.fechar.Text = "Fechar";
            this.fechar.Click += new System.EventHandler(this.fechar_Click);
            // 
            // atualizar
            // 
            this.atualizar.BackColor = System.Drawing.Color.OliveDrab;
            this.atualizar.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.atualizar.ForeColor = System.Drawing.Color.White;
            this.atualizar.Location = new System.Drawing.Point(119, 4);
            this.atualizar.Name = "atualizar";
            this.atualizar.Size = new System.Drawing.Size(118, 110);
            this.atualizar.TabIndex = 8;
            this.atualizar.Text = "Atualizar";
            this.atualizar.Click += new System.EventHandler(this.atualizar_Click);
            // 
            // inventario
            // 
            this.inventario.BackColor = System.Drawing.Color.Red;
            this.inventario.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.inventario.ForeColor = System.Drawing.Color.White;
            this.inventario.Location = new System.Drawing.Point(0, 4);
            this.inventario.Name = "inventario";
            this.inventario.Size = new System.Drawing.Size(118, 110);
            this.inventario.TabIndex = 7;
            this.inventario.Text = "Inventário";
            this.inventario.Click += new System.EventHandler(this.inventario_Click);
            // 
            // Inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(237, 231);
            this.ControlBox = false;
            this.Controls.Add(this.atualizar);
            this.Controls.Add(this.inventario);
            this.Controls.Add(this.checkout);
            this.Controls.Add(this.fechar);
            this.MinimizeBox = false;
            this.Name = "Inicio";
            this.Text = "RFLOG - Inicio";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button checkout;
        private System.Windows.Forms.Button fechar;
        private System.Windows.Forms.Button atualizar;
        private System.Windows.Forms.Button inventario;
    }
}