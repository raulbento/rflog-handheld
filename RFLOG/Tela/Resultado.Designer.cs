﻿namespace RFLOG.Tela
{
    /// <summary>
    ///     Tela que mostra um resumo da leitura executada.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    partial class Resultado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbQuantidadeProduto = new System.Windows.Forms.Label();
            this.quantidadeProdutos = new System.Windows.Forms.Label();
            this.lbDataInicio = new System.Windows.Forms.Label();
            this.lbQuantidadeTags = new System.Windows.Forms.Label();
            this.lbDataTermino = new System.Windows.Forms.Label();
            this.quantidadeTags = new System.Windows.Forms.Label();
            this.dataTermino = new System.Windows.Forms.Label();
            this.dataInicio = new System.Windows.Forms.Label();
            this.end = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.labelGrupo = new System.Windows.Forms.Label();
            this.grupo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.estabelecimento = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbQuantidadeProduto
            // 
            this.lbQuantidadeProduto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lbQuantidadeProduto.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbQuantidadeProduto.Location = new System.Drawing.Point(3, 3);
            this.lbQuantidadeProduto.Name = "lbQuantidadeProduto";
            this.lbQuantidadeProduto.Size = new System.Drawing.Size(115, 33);
            this.lbQuantidadeProduto.Text = "Quantidade Produtos:";
            // 
            // quantidadeProdutos
            // 
            this.quantidadeProdutos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.quantidadeProdutos.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.quantidadeProdutos.Location = new System.Drawing.Point(3, 36);
            this.quantidadeProdutos.Name = "quantidadeProdutos";
            this.quantidadeProdutos.Size = new System.Drawing.Size(115, 22);
            this.quantidadeProdutos.Text = "0";
            this.quantidadeProdutos.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbDataInicio
            // 
            this.lbDataInicio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lbDataInicio.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbDataInicio.Location = new System.Drawing.Point(3, 59);
            this.lbDataInicio.Name = "lbDataInicio";
            this.lbDataInicio.Size = new System.Drawing.Size(115, 44);
            this.lbDataInicio.Text = "Data (Início):";
            // 
            // lbQuantidadeTags
            // 
            this.lbQuantidadeTags.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lbQuantidadeTags.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbQuantidadeTags.Location = new System.Drawing.Point(119, 3);
            this.lbQuantidadeTags.Name = "lbQuantidadeTags";
            this.lbQuantidadeTags.Size = new System.Drawing.Size(115, 33);
            this.lbQuantidadeTags.Text = "Quantidade Tags:";
            // 
            // lbDataTermino
            // 
            this.lbDataTermino.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lbDataTermino.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbDataTermino.Location = new System.Drawing.Point(119, 59);
            this.lbDataTermino.Name = "lbDataTermino";
            this.lbDataTermino.Size = new System.Drawing.Size(115, 44);
            this.lbDataTermino.Text = "Data (Termino):";
            // 
            // quantidadeTags
            // 
            this.quantidadeTags.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.quantidadeTags.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.quantidadeTags.Location = new System.Drawing.Point(119, 36);
            this.quantidadeTags.Name = "quantidadeTags";
            this.quantidadeTags.Size = new System.Drawing.Size(115, 22);
            this.quantidadeTags.Text = "0";
            this.quantidadeTags.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dataTermino
            // 
            this.dataTermino.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataTermino.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.dataTermino.Location = new System.Drawing.Point(119, 103);
            this.dataTermino.Name = "dataTermino";
            this.dataTermino.Size = new System.Drawing.Size(115, 20);
            this.dataTermino.Text = "24/02/2016";
            this.dataTermino.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dataInicio
            // 
            this.dataInicio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataInicio.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.dataInicio.Location = new System.Drawing.Point(3, 103);
            this.dataInicio.Name = "dataInicio";
            this.dataInicio.Size = new System.Drawing.Size(115, 20);
            this.dataInicio.Text = "24/02/2016";
            this.dataInicio.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // end
            // 
            this.end.BackColor = System.Drawing.Color.DodgerBlue;
            this.end.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.end.ForeColor = System.Drawing.SystemColors.Control;
            this.end.Location = new System.Drawing.Point(119, 195);
            this.end.Name = "end";
            this.end.Size = new System.Drawing.Size(118, 36);
            this.end.TabIndex = 9;
            this.end.Text = "Concluir";
            this.end.Click += new System.EventHandler(this.end_Click);
            // 
            // back
            // 
            this.back.BackColor = System.Drawing.Color.Red;
            this.back.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.back.ForeColor = System.Drawing.SystemColors.Control;
            this.back.Location = new System.Drawing.Point(0, 195);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(118, 36);
            this.back.TabIndex = 8;
            this.back.Text = "<";
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // labelGrupo
            // 
            this.labelGrupo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelGrupo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelGrupo.Location = new System.Drawing.Point(3, 124);
            this.labelGrupo.Name = "labelGrupo";
            this.labelGrupo.Size = new System.Drawing.Size(115, 44);
            this.labelGrupo.Text = "Grupo";
            // 
            // grupo
            // 
            this.grupo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.grupo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.grupo.Location = new System.Drawing.Point(3, 168);
            this.grupo.Name = "grupo";
            this.grupo.Size = new System.Drawing.Size(115, 20);
            this.grupo.Text = "Grupo 01";
            this.grupo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(119, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 44);
            this.label3.Text = "Estabelecimento";
            // 
            // estabelecimento
            // 
            this.estabelecimento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.estabelecimento.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.estabelecimento.Location = new System.Drawing.Point(119, 168);
            this.estabelecimento.Name = "estabelecimento";
            this.estabelecimento.Size = new System.Drawing.Size(115, 20);
            this.estabelecimento.Text = "Loja 01";
            this.estabelecimento.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Resultado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(237, 231);
            this.ControlBox = false;
            this.Controls.Add(this.estabelecimento);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.grupo);
            this.Controls.Add(this.labelGrupo);
            this.Controls.Add(this.end);
            this.Controls.Add(this.back);
            this.Controls.Add(this.dataInicio);
            this.Controls.Add(this.dataTermino);
            this.Controls.Add(this.quantidadeTags);
            this.Controls.Add(this.lbDataTermino);
            this.Controls.Add(this.lbQuantidadeTags);
            this.Controls.Add(this.lbDataInicio);
            this.Controls.Add(this.quantidadeProdutos);
            this.Controls.Add(this.lbQuantidadeProduto);
            this.MinimizeBox = false;
            this.Name = "Resultado";
            this.Text = "RFLOG - Resultado";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbQuantidadeProduto;
        private System.Windows.Forms.Label quantidadeProdutos;
        private System.Windows.Forms.Label lbDataInicio;
        private System.Windows.Forms.Label lbQuantidadeTags;
        private System.Windows.Forms.Label lbDataTermino;
        private System.Windows.Forms.Label quantidadeTags;
        private System.Windows.Forms.Label dataTermino;
        private System.Windows.Forms.Label dataInicio;
        private System.Windows.Forms.Button end;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Label labelGrupo;
        private System.Windows.Forms.Label grupo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label estabelecimento;
    }
}