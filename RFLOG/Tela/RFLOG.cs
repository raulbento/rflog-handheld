﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using RFLOG.Util;
using System.Xml;

namespace RFLOG.Tela
{
    /// <summary>
    ///     Tela Principal e de Inicialização do Sistema.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    
    public partial class RFLOG : Form
    {

        private Resultado resultado;

        private Leitura leitura;

        private Selecionar selecionar;

        private Inicio inicio;

        private AlertaLeitura alertaLeitura;

        public RFLOG()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            Selecionar.RFLOG = this;
            Resultado.RFLOG = this;
            Leitura.RFLOG = this;
            Inicio.RFLOG = this;
            AlertaLeitura.RFLOG = this;
        }

        public void clear()
        {

        }

        private void load_Tick(object sender, EventArgs e)
        {
            try
            {
                Inicio.Show();
            }
            finally
            {
                load.Enabled = false;
            }
        }

        public Inicio Inicio
        {
            get
            {
                if (inicio == null)
                {
                    inicio = new Inicio();
                }
                return inicio;
            }

            set
            {
                inicio = value;
            }
        }

        public Resultado Resultado
        {
            get
            {
                if (resultado == null)
                {
                    resultado = new Resultado();
                }
                return resultado;
            }

            set
            {
                resultado = value;
            }
        }

        public Leitura Leitura
        {
            get
            {
                if (leitura == null)
                {
                    leitura = new Leitura();
                }
                return leitura;
            }

            set
            {
                leitura = value;
            }
        }

        public Selecionar Selecionar
        {
            get
            {
                if (selecionar == null)
                {
                    selecionar = new Selecionar();
                }
                return selecionar;
            }

            set
            {
                selecionar = value;
            }
        }

        public AlertaLeitura AlertaLeitura
        {
            get
            {
                if (alertaLeitura == null)
                {
                    alertaLeitura = new AlertaLeitura();
                }
                return alertaLeitura;
            }

            set
            {
                alertaLeitura = value;
            }
        }

        public void getEstabelecimentoList()
        {
            if (FileUtil.SaveXML(RestClient.MakeRequest(Mensagem.LISTAR_ESTABELECIMENTO_WEB_SERVICE, "GET", null)))
            {
                MessageBox.Show(Mensagem.XML_SUCESSO, Mensagem.ST_SUCESSO_UP, MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
            else
            {
                MessageBox.Show(Mensagem.XML_FALHA, Mensagem.ST_ERRO_UP, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        } 
    }
}