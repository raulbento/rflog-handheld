﻿namespace RFLOG.Tela
{
    /// <summary>
    ///     Tela de Seleção de Estabelecimento e Setor.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    partial class Selecionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.next = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.estabelecimento = new System.Windows.Forms.ComboBox();
            this.labelEstabelecimento = new System.Windows.Forms.Label();
            this.labelSetor = new System.Windows.Forms.Label();
            this.setor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // next
            // 
            this.next.BackColor = System.Drawing.Color.DodgerBlue;
            this.next.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.next.ForeColor = System.Drawing.SystemColors.Control;
            this.next.Location = new System.Drawing.Point(119, 195);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(118, 36);
            this.next.TabIndex = 4;
            this.next.Text = ">";
            this.next.Click += new System.EventHandler(this.next_Click);
            // 
            // back
            // 
            this.back.BackColor = System.Drawing.Color.Red;
            this.back.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.back.ForeColor = System.Drawing.SystemColors.Control;
            this.back.Location = new System.Drawing.Point(0, 195);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(118, 36);
            this.back.TabIndex = 3;
            this.back.Text = "<";
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // estabelecimento
            // 
            this.estabelecimento.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.estabelecimento.Location = new System.Drawing.Point(4, 41);
            this.estabelecimento.Name = "estabelecimento";
            this.estabelecimento.Size = new System.Drawing.Size(230, 27);
            this.estabelecimento.TabIndex = 5;
            this.estabelecimento.SelectedIndexChanged += new System.EventHandler(this.estabelecimento_SelectedIndexChanged);
            // 
            // labelEstabelecimento
            // 
            this.labelEstabelecimento.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelEstabelecimento.Location = new System.Drawing.Point(4, 18);
            this.labelEstabelecimento.Name = "labelEstabelecimento";
            this.labelEstabelecimento.Size = new System.Drawing.Size(230, 20);
            this.labelEstabelecimento.Text = "Estabelecimento";
            // 
            // labelSetor
            // 
            this.labelSetor.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelSetor.Location = new System.Drawing.Point(4, 71);
            this.labelSetor.Name = "labelSetor";
            this.labelSetor.Size = new System.Drawing.Size(230, 20);
            this.labelSetor.Text = "Setor";
            // 
            // setor
            // 
            this.setor.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.setor.Location = new System.Drawing.Point(4, 94);
            this.setor.Name = "setor";
            this.setor.Size = new System.Drawing.Size(230, 27);
            this.setor.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(4, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 20);
            this.label1.Text = "Grupo";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.comboBox1.Location = new System.Drawing.Point(4, 148);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(230, 27);
            this.comboBox1.TabIndex = 10;
            // 
            // Selecionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(237, 231);
            this.ControlBox = false;
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.setor);
            this.Controls.Add(this.labelSetor);
            this.Controls.Add(this.labelEstabelecimento);
            this.Controls.Add(this.estabelecimento);
            this.Controls.Add(this.next);
            this.Controls.Add(this.back);
            this.MinimizeBox = false;
            this.Name = "Selecionar";
            this.Text = "RFLOG - Selecionar";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button next;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.ComboBox estabelecimento;
        private System.Windows.Forms.Label labelEstabelecimento;
        private System.Windows.Forms.Label labelSetor;
        private System.Windows.Forms.ComboBox setor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}