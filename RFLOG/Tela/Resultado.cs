﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RFLOG.Tela
{
    /// <summary>
    ///     Tela que mostra um resumo da leitura executada.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    
    public partial class Resultado : Form
    {

        private RFLOG rflog;

        public Resultado()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
        }

        private void end_Click(object sender, EventArgs e)
        {
            RFLOG.Selecionar.Show();
        }

        private void back_Click(object sender, EventArgs e)
        {
            RFLOG.Leitura.Show();
        }

        public void clear()
        {

        }

        public RFLOG RFLOG
        {
            get
            {
                if (rflog == null)
                {
                    rflog = new RFLOG();
                }
                return rflog;
            }

            set
            {
                rflog = value;
            }
        }
    }
}