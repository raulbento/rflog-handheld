﻿namespace RFLOG.Tela
{
    /// <summary>
    ///     Tela Principal e de Inicialização do Sistema.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    partial class RFLOG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.title = new System.Windows.Forms.Label();
            this.subtitle = new System.Windows.Forms.Label();
            this.version = new System.Windows.Forms.Label();
            this.author = new System.Windows.Forms.Label();
            this.load = new System.Windows.Forms.Timer();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Font = new System.Drawing.Font("Courier New", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.title.ForeColor = System.Drawing.Color.Red;
            this.title.Location = new System.Drawing.Point(8, 47);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(220, 71);
            this.title.Text = "RFLOG";
            // 
            // subtitle
            // 
            this.subtitle.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.subtitle.ForeColor = System.Drawing.Color.OliveDrab;
            this.subtitle.Location = new System.Drawing.Point(3, 15);
            this.subtitle.Name = "subtitle";
            this.subtitle.Size = new System.Drawing.Size(115, 26);
            this.subtitle.Text = "Handheld";
            // 
            // version
            // 
            this.version.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.version.ForeColor = System.Drawing.Color.DodgerBlue;
            this.version.Location = new System.Drawing.Point(173, 131);
            this.version.Name = "version";
            this.version.Size = new System.Drawing.Size(64, 20);
            this.version.Text = "v. 1.0a";
            // 
            // author
            // 
            this.author.ForeColor = System.Drawing.Color.DarkOrange;
            this.author.Location = new System.Drawing.Point(77, 211);
            this.author.Name = "author";
            this.author.Size = new System.Drawing.Size(83, 20);
            this.author.Text = "by Raul Bento";
            // 
            // load
            // 
            this.load.Enabled = true;
            this.load.Interval = 5000;
            this.load.Tick += new System.EventHandler(this.load_Tick);
            // 
            // RFLOG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(237, 231);
            this.ControlBox = false;
            this.Controls.Add(this.author);
            this.Controls.Add(this.version);
            this.Controls.Add(this.subtitle);
            this.Controls.Add(this.title);
            this.MinimizeBox = false;
            this.Name = "RFLOG";
            this.Text = "RFLOG";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Label subtitle;
        private System.Windows.Forms.Label version;
        private System.Windows.Forms.Label author;
        private System.Windows.Forms.Timer load;
    }
}