﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using RFLOG.Util;

namespace RFLOG.Model
{
    class Tag
    {

        public long id { get; set; }

        public string epc { get; set; }

        public Setor setor { get; set; }

        public bool registroAtivo { get; set; }

        public Tag()
        {
        }

        public Tag(XmlNode xmlNode)
        {
            foreach (XmlNode node in xmlNode.ChildNodes)
            {
                if ("id".Equals(node.Name))
                {
                    this.id = long.Parse(node.InnerText);
                }
                if ("epc".Equals(node.Name))
                {
                    this.epc = node.InnerText;
                }
                if ("setor".Equals(node.Name))
                {
                    this.setor = new Setor(node);
                }
                if ("registroAtivo".ToLower().Equals(node.Name.ToLower()))
                {
                    this.registroAtivo = bool.Parse(node.InnerText);
                }
            }
        }

        public XmlDocument getXML()
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(this.ToString());
            return xml;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<").Append(this.GetType().Name.ToLower()).Append(">");
            foreach (PropertyInfo property in this.GetType().GetProperties())
            {
                if ("setor".Equals(property.Name.ToLower()))
                {
                    sb.Append(setor.ToString());
                }
                else
                {
                    sb.Append("<").Append(property.Name).Append(">");
                    sb.Append(property.GetValue(this, null));
                    sb.Append("</").Append(property.Name).Append(">");
                }
            }
            sb.Append("</").Append(this.GetType().Name.ToLower()).Append(">");
            return sb.ToString();
        }
    }
}
