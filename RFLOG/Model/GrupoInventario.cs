﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using RFLOG.Util;

namespace RFLOG.Model
{
    class GrupoInventario
    {
        public long id { get; set; }

        public DateTime criacao { get; set; }

        public Estabelecimento estabelecimento { get; set; }

        public bool completo { get; set; }

        public List<Inventario> inventarios { get; set; }

        public bool registroAtivo { get; set; }

        public GrupoInventario()
        {
        }

        public GrupoInventario(XmlNode xmlNode)
        {
            foreach (XmlNode node in xmlNode.ChildNodes)
            {
                if ("id".Equals(node.Name))
                {
                    this.id = long.Parse(node.InnerText);
                }
                if ("criacao".Equals(node.Name))
                {
                    this.criacao = DateTime.Parse(node.InnerText);
                }
                if ("registroAtivo".Equals(node.Name))
                {
                    this.registroAtivo = bool.Parse(node.InnerText);
                }
                if ("completo".Equals(node.Name))
                {
                    this.completo = bool.Parse(node.InnerText);
                }
                if ("estabelecimento".Equals(node.Name))
                {
                    estabelecimento = new Estabelecimento(node);
                }
                if ("inventarios".Equals(node.Name))
                {
                    if (inventarios == null)
                    {
                        inventarios = new List<Inventario>();
                    }
                    foreach (XmlNode tagNode in node.ChildNodes)
                    {
                        this.inventarios.Add(new Inventario(tagNode));
                    }
                }
            }
        }

        public XmlDocument getXML()
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(this.ToString());
            return xml;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<").Append(this.GetType().Name.Substring(0, 1).ToLower()).Append(this.GetType().Name.Substring(1, this.GetType().Name.Length)).Append(">");
            foreach (PropertyInfo property in this.GetType().GetProperties())
            {
                if ("inventarios".Equals(property.Name.ToLower()))
                {
                    sb.Append("<").Append(property.Name).Append(">");
                    foreach (var inventario in inventarios)
                    {
                        sb.Append(inventario.ToString());
                    }
                    sb.Append("</").Append(property.Name).Append(">");
                }
                else if ("estabelecimento".Equals(property.Name.ToLower()))
                {
                    sb.Append(estabelecimento.ToString());
                }
                else
                {
                    sb.Append("<").Append(property.Name).Append(">");
                    sb.Append(property.GetValue(this, null));
                    sb.Append("</").Append(property.Name).Append(">");
                }
            }
            sb.Append("</").Append(this.GetType().Name.Substring(0, 1).ToLower()).Append(this.GetType().Name.Substring(1, this.GetType().Name.Length)).Append(">");
            return sb.ToString();
        }
    }
}
